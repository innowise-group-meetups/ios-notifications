//
//  NotificationViewController.swift
//  NotificationContentExtension
//
//  Created by mac on 26.01.2021.
//

import UIKit
import UserNotifications
import UserNotificationsUI

class NotificationViewController: UIViewController, UNNotificationContentExtension {

    @IBOutlet weak var bodyLabel: UILabel!
    @IBOutlet weak var noButton: UIButton!
    @IBOutlet weak var yesButton: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        yesButton.setTitle("YES", for: .normal)
        noButton.setTitle("NO", for: .normal)
    }
    @IBAction func onYesButton(_ sender: UIButton) {
        self.bodyLabel?.text = "YES"
    }
    @IBAction func onNoButton(_ sender: Any) {
        self.bodyLabel?.text = "NO"
    }
    
    func didReceive(_ notification: UNNotification) {
        self.bodyLabel?.text = notification.request.content.body
    }
}
