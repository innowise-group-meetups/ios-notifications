//
//  NotificationManager.swift
//  MeetupNotifications
//
//  Created by mac on 21.01.2021.
//

import Foundation
import UserNotifications
import UIKit

class NotificationManager: NSObject {
    
    let notificationCenter = UNUserNotificationCenter.current()
    
    override init() {
        super.init()
        notificationCenter.delegate = self
    }
    
    func userRequest() {
        let options: UNAuthorizationOptions = [.alert, .sound, .badge]
        //let options: UNAuthorizationOptions = [.alert, .sound, .badge, .provisional]

        notificationCenter.requestAuthorization(options: options) { [weak self] granted, _ in
            print("Permission granted: \(granted)")
            guard let self = self, granted else { return }
            self.getNotificationSettings()
        }
    }
    
    func scheduleNotification() {
        
        let content = UNMutableNotificationContent() // 1 step
        let userActions = "User Actions"
        
        content.title = "Example"
        content.body = "This is example notification"
        content.sound = UNNotificationSound.default
        content.categoryIdentifier = userActions
        
        let trigger = UNTimeIntervalNotificationTrigger(timeInterval: 5, repeats: false) // 2 step
    
        let identifier = "Local Notification" // 3 step
        let request = UNNotificationRequest(identifier: identifier, content: content, trigger: trigger)
        
        notificationCenter.add(request) { (error) in
            if let error = error {
                print("Error \(error.localizedDescription)")
            }
        }
        
        let repeatAction = UNNotificationAction(identifier: "Repeat", title: "Repeat", options: [])
        let deleteAction = UNNotificationAction(identifier: "Delete", title: "Delete", options: [.destructive])
        let replyAction = UNTextInputNotificationAction(identifier: "Reply", title: "Reply", options: [], textInputButtonTitle: "Answer", textInputPlaceholder: "")
        let category = UNNotificationCategory(identifier: userActions,
                                              actions: [repeatAction, replyAction , deleteAction],
                                              intentIdentifiers: [],
                                              options: [])
        
        notificationCenter.setNotificationCategories([category])
    }
    
    func getNotificationSettings() {
        notificationCenter.getNotificationSettings { settings in
            print("Notification settings: \(settings)")
            guard settings.authorizationStatus == .authorized else { return }
            DispatchQueue.main.async {
                UIApplication.shared.registerForRemoteNotifications()
            }
        }
    }
}

extension NotificationManager:  UNUserNotificationCenterDelegate {
    func userNotificationCenter(_ center: UNUserNotificationCenter,
                                willPresent notification: UNNotification,
                                withCompletionHandler completionHandler: @escaping (UNNotificationPresentationOptions) -> Void) {
        
        completionHandler([.alert, .sound])
    }
    
    func userNotificationCenter(_ center: UNUserNotificationCenter,
                                didReceive response: UNNotificationResponse,
                                withCompletionHandler completionHandler: @escaping () -> Void) {
        
        switch response.actionIdentifier {
        case UNNotificationDismissActionIdentifier:
            print("Dismiss Action")
        case UNNotificationDefaultActionIdentifier:
            print("Default")
        case "Repeat":
            print("Repeat")
            scheduleNotification()
        case "Delete":
            print("Delete")
        case "Reply":
            if let textResponse = response as? UNTextInputNotificationResponse {
                print("Text: \(textResponse.userText)")
            }
        default:
            print("Unknown action")
        }
        completionHandler()
    }
}
