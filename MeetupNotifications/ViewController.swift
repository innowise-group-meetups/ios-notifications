//
//  ViewController.swift
//  MeetupNotifications
//
//  Created by mac on 21.01.2021.
//

import UIKit
import SnapKit

class ViewController: UIViewController {
    private let notificationManager: NotificationManager
    
    private let registerNotificationButton: UIButton = {
        let button = UIButton()
        button.setTitle("Register local notification", for: .normal)
        button.setTitleColor(.white, for: .normal)
        button.backgroundColor = .systemBlue
        button.layer.cornerRadius = 15
        button.addTarget(self, action: #selector(registerNotification), for: .touchUpInside)
        return button
    }()
    
    init(notificationManager: NotificationManager) {
        self.notificationManager = notificationManager
        super.init(nibName: nil, bundle: nil)
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        view.backgroundColor = .white
        view.addSubview(registerNotificationButton)
        
        registerNotificationButton.snp.makeConstraints {
            $0.centerX.centerY.equalToSuperview()
            $0.leading.equalToSuperview().offset(20)
            $0.trailing.equalToSuperview().offset(-20)
            $0.height.equalTo(70)
        }
    }
}

private extension ViewController {
    @objc func registerNotification() {
        notificationManager.scheduleNotification()
    }
}
