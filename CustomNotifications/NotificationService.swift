//
//  NotificationService.swift
//  CustomNotifications
//
//  Created by mac on 25.01.2021.
//

import UserNotifications

class NotificationService: UNNotificationServiceExtension {

    var contentHandler: ((UNNotificationContent) -> Void)?
    var bestAttemptContent: UNMutableNotificationContent?

    override func didReceive(_ request: UNNotificationRequest, withContentHandler contentHandler: @escaping (UNNotificationContent) -> Void) {
        defer { contentHandler(bestAttemptContent ?? request.content) }
        
        self.contentHandler = contentHandler
        bestAttemptContent = (request.content.mutableCopy() as? UNMutableNotificationContent)
        
        guard let richContent = request.content.userInfo["rich-data"] as? [String: String] else { return }
        
        var attachments = [UNNotificationAttachment]()
        if let attachmentString = richContent["image"], let attachment = loadAttachment(urlString: attachmentString, isThumbnail: false) {
            attachments.append(attachment)
        }
        if let attachmentString = richContent["icon"], let attachment = loadAttachment(urlString: attachmentString, isThumbnail: true) {
            attachments.append(attachment)
        }
        
        bestAttemptContent?.attachments = attachments
    }
    
    override func serviceExtensionTimeWillExpire() {
        if let contentHandler = contentHandler, let bestAttemptContent =  bestAttemptContent {
            contentHandler(bestAttemptContent)
        }
    }
    
    func loadAttachment(urlString: String, isThumbnail: Bool) -> UNNotificationAttachment? {
        guard let url = URL(string: urlString) else { return nil }
        
        let fileManager = FileManager.default
        let tempFolderName = ProcessInfo.processInfo.globallyUniqueString
        let tempFolderURL = URL(fileURLWithPath: NSTemporaryDirectory())
            .appendingPathComponent(tempFolderName, isDirectory: true)
        do {
            try fileManager.createDirectory(at: tempFolderURL, withIntermediateDirectories: true, attributes: nil)
            let fileURL = tempFolderURL.appendingPathExtension(url.pathExtension)
            let imageData = try Data(contentsOf: url)
            try imageData.write(to: fileURL)
            return try UNNotificationAttachment(identifier: urlString, url: fileURL, options: [UNNotificationAttachmentOptionsThumbnailHiddenKey : !isThumbnail])
        } catch {
            print(error)
            return nil
        }
    }
}
